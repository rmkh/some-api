/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var SomeApiComponent = /** @class */ (function () {
    function SomeApiComponent() {
    }
    /**
     * @return {?}
     */
    SomeApiComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    SomeApiComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-some-api',
                    template: "\n    <p>\n      some-api works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    SomeApiComponent.ctorParameters = function () { return []; };
    return SomeApiComponent;
}());
export { SomeApiComponent };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29tZS1hcGkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29tZS1hcGkvIiwic291cmNlcyI6WyJsaWIvc29tZS1hcGkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDOztJQWFoRDtLQUFpQjs7OztJQUVqQixtQ0FBUTs7O0lBQVI7S0FDQzs7Z0JBZEYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxjQUFjO29CQUN4QixRQUFRLEVBQUUsZ0RBSVQ7aUJBRUY7Ozs7MkJBVkQ7O1NBV2EsZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLXNvbWUtYXBpJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8cD5cbiAgICAgIHNvbWUtYXBpIHdvcmtzIVxuICAgIDwvcD5cbiAgYCxcbiAgc3R5bGVzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBTb21lQXBpQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbn1cbiJdfQ==