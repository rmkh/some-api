/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { SomeApiComponent } from './some-api.component';
import { ButtonComponent } from './button/button.component';
var SomeApiModule = /** @class */ (function () {
    function SomeApiModule() {
    }
    SomeApiModule.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    declarations: [SomeApiComponent, ButtonComponent],
                    exports: [SomeApiComponent]
                },] }
    ];
    return SomeApiModule;
}());
export { SomeApiModule };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29tZS1hcGkubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29tZS1hcGkvIiwic291cmNlcyI6WyJsaWIvc29tZS1hcGkubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQzs7Ozs7Z0JBRTNELFFBQVEsU0FBQztvQkFDUixPQUFPLEVBQUUsRUFDUjtvQkFDRCxZQUFZLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxlQUFlLENBQUM7b0JBQ2pELE9BQU8sRUFBRSxDQUFDLGdCQUFnQixDQUFDO2lCQUM1Qjs7d0JBVEQ7O1NBVWEsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTb21lQXBpQ29tcG9uZW50IH0gZnJvbSAnLi9zb21lLWFwaS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQnV0dG9uQ29tcG9uZW50IH0gZnJvbSAnLi9idXR0b24vYnV0dG9uLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbU29tZUFwaUNvbXBvbmVudCwgQnV0dG9uQ29tcG9uZW50XSxcbiAgZXhwb3J0czogW1NvbWVBcGlDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFNvbWVBcGlNb2R1bGUgeyB9XG4iXX0=