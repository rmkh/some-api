/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/*
 * Public API Surface of some-api
 */
export { SomeApiService } from './lib/some-api.service';
export { SomeApiComponent } from './lib/some-api.component';
export { SomeApiModule } from './lib/some-api.module';

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NvbWUtYXBpLyIsInNvdXJjZXMiOlsicHVibGljX2FwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBSUEsK0JBQWMsd0JBQXdCLENBQUM7QUFDdkMsaUNBQWMsMEJBQTBCLENBQUM7QUFDekMsOEJBQWMsdUJBQXVCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogUHVibGljIEFQSSBTdXJmYWNlIG9mIHNvbWUtYXBpXG4gKi9cblxuZXhwb3J0ICogZnJvbSAnLi9saWIvc29tZS1hcGkuc2VydmljZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9zb21lLWFwaS5jb21wb25lbnQnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvc29tZS1hcGkubW9kdWxlJztcbiJdfQ==