(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('some-api', ['exports', '@angular/core'], factory) :
    (factory((global['some-api'] = {}),global.ng.core));
}(this, (function (exports,i0) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var SomeApiService = /** @class */ (function () {
        function SomeApiService() {
        }
        SomeApiService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        SomeApiService.ctorParameters = function () { return []; };
        /** @nocollapse */ SomeApiService.ngInjectableDef = i0.defineInjectable({ factory: function SomeApiService_Factory() { return new SomeApiService(); }, token: SomeApiService, providedIn: "root" });
        return SomeApiService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var SomeApiComponent = /** @class */ (function () {
        function SomeApiComponent() {
        }
        /**
         * @return {?}
         */
        SomeApiComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        SomeApiComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'lib-some-api',
                        template: "\n    <p>\n      some-api works!\n    </p>\n  "
                    }] }
        ];
        /** @nocollapse */
        SomeApiComponent.ctorParameters = function () { return []; };
        return SomeApiComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var ButtonComponent = /** @class */ (function () {
        function ButtonComponent() {
        }
        /**
         * @return {?}
         */
        ButtonComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        ButtonComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'lib-button',
                        template: "<p>\n  button works!\n</p>\n",
                        styles: [""]
                    }] }
        ];
        /** @nocollapse */
        ButtonComponent.ctorParameters = function () { return []; };
        return ButtonComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var SomeApiModule = /** @class */ (function () {
        function SomeApiModule() {
        }
        SomeApiModule.decorators = [
            { type: i0.NgModule, args: [{
                        imports: [],
                        declarations: [SomeApiComponent, ButtonComponent],
                        exports: [SomeApiComponent]
                    },] }
        ];
        return SomeApiModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    exports.SomeApiService = SomeApiService;
    exports.SomeApiComponent = SomeApiComponent;
    exports.SomeApiModule = SomeApiModule;
    exports.ɵa = ButtonComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=some-api.umd.js.map