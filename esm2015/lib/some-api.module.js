/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { SomeApiComponent } from './some-api.component';
import { ButtonComponent } from './button/button.component';
export class SomeApiModule {
}
SomeApiModule.decorators = [
    { type: NgModule, args: [{
                imports: [],
                declarations: [SomeApiComponent, ButtonComponent],
                exports: [SomeApiComponent]
            },] }
];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29tZS1hcGkubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc29tZS1hcGkvIiwic291cmNlcyI6WyJsaWIvc29tZS1hcGkubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQVE1RCxNQUFNLE9BQU8sYUFBYTs7O1lBTnpCLFFBQVEsU0FBQztnQkFDUixPQUFPLEVBQUUsRUFDUjtnQkFDRCxZQUFZLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxlQUFlLENBQUM7Z0JBQ2pELE9BQU8sRUFBRSxDQUFDLGdCQUFnQixDQUFDO2FBQzVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFNvbWVBcGlDb21wb25lbnQgfSBmcm9tICcuL3NvbWUtYXBpLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBCdXR0b25Db21wb25lbnQgfSBmcm9tICcuL2J1dHRvbi9idXR0b24uY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtTb21lQXBpQ29tcG9uZW50LCBCdXR0b25Db21wb25lbnRdLFxuICBleHBvcnRzOiBbU29tZUFwaUNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgU29tZUFwaU1vZHVsZSB7IH1cbiJdfQ==