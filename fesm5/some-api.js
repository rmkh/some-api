import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var SomeApiService = /** @class */ (function () {
    function SomeApiService() {
    }
    SomeApiService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SomeApiService.ctorParameters = function () { return []; };
    /** @nocollapse */ SomeApiService.ngInjectableDef = defineInjectable({ factory: function SomeApiService_Factory() { return new SomeApiService(); }, token: SomeApiService, providedIn: "root" });
    return SomeApiService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var SomeApiComponent = /** @class */ (function () {
    function SomeApiComponent() {
    }
    /**
     * @return {?}
     */
    SomeApiComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    SomeApiComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-some-api',
                    template: "\n    <p>\n      some-api works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    SomeApiComponent.ctorParameters = function () { return []; };
    return SomeApiComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var ButtonComponent = /** @class */ (function () {
    function ButtonComponent() {
    }
    /**
     * @return {?}
     */
    ButtonComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    ButtonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-button',
                    template: "<p>\n  button works!\n</p>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    ButtonComponent.ctorParameters = function () { return []; };
    return ButtonComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var SomeApiModule = /** @class */ (function () {
    function SomeApiModule() {
    }
    SomeApiModule.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    declarations: [SomeApiComponent, ButtonComponent],
                    exports: [SomeApiComponent]
                },] }
    ];
    return SomeApiModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

export { SomeApiService, SomeApiComponent, SomeApiModule, ButtonComponent as ɵa };

//# sourceMappingURL=some-api.js.map