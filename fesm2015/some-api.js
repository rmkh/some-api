import { Injectable, Component, NgModule, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class SomeApiService {
    constructor() { }
}
SomeApiService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SomeApiService.ctorParameters = () => [];
/** @nocollapse */ SomeApiService.ngInjectableDef = defineInjectable({ factory: function SomeApiService_Factory() { return new SomeApiService(); }, token: SomeApiService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class SomeApiComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
SomeApiComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-some-api',
                template: `
    <p>
      some-api works!
    </p>
  `
            }] }
];
/** @nocollapse */
SomeApiComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class ButtonComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
ButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-button',
                template: "<p>\n  button works!\n</p>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
ButtonComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class SomeApiModule {
}
SomeApiModule.decorators = [
    { type: NgModule, args: [{
                imports: [],
                declarations: [SomeApiComponent, ButtonComponent],
                exports: [SomeApiComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

export { SomeApiService, SomeApiComponent, SomeApiModule, ButtonComponent as ɵa };

//# sourceMappingURL=some-api.js.map